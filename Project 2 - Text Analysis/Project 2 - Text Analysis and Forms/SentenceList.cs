﻿//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//	Project:		Project 2 - Text Analysis and Forms
//	File Name:		SentenceList.cs
//	Description:    Stores information about all of the sentences within the program and deals with the group aspects of the sentences.
//	Course:			CSCI 2210-001 - Data Structures
//	Author:			Brandi Campbell, campbellb1@goldmail.etsu.edu
//	Created:		Thursday, February 21, 2016
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////




using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Utils;


namespace TextAnalyzer
{


    /// <summary>
    /// This class contains a list of Sentences and manages them and their group info.
    /// </summary>
    class SentenceList
    {
        MatchCollection matchList;
        Sentence sentenceToAdd;
        private int wordPos = 0;
        public int numSentences = 0;
        private double avgLength = 0;
        public List<Sentence> sList = new List<Sentence>(); //will hold the list of sentences
        public string content;

        /// <summary>
        /// Gets or sets the number sentences.
        /// </summary>
        /// <value>
        /// The number sentences.
        /// </value>
        public int NumSentences
        {
            get
            {
                return numSentences;
            }

            private set
            {
                numSentences = value;
            }
        }


        /// <summary>
        /// Gets or sets the content.
        /// </summary>
        /// <value>
        /// The content.
        /// </value>
        public string Content
        {
            get
            {
                return content;
            }

            set
            {
                content = value;
            }
        }


        /// <summary>
        /// Gets or sets the s list.
        /// </summary>
        /// <value>
        /// The s list.
        /// </value>
        internal List<Sentence> SList
        {
            get
            {
                return sList;
            }

            private set
            {
                sList = value;
            }
        }


        /// <summary>
        /// Gets or sets the word position.
        /// </summary>
        /// <value>
        /// The word position.
        /// </value>
        public int WordPos
        {
            get
            {
                return wordPos;
            }

            set
            {
                wordPos = value;
            }
        }


        /// <summary>
        /// Gets the average length.
        /// </summary>
        /// <value>
        /// The average length.
        /// </value>
        public double AvgLength
        {
            get
            {
                return avgLength;
            }

            private set
            {
                avgLength = value;
            }
        }



        /// <summary>
        /// Initializes a new instance of the <see cref="SentenceList"/> class.
        /// </summary>
        /// <param name="input">The input.</param>
        public SentenceList()
        {
            matchList = null;
            wordPos = 0;
            numSentences = 0;
            avgLength = 0;
            sList = new List<Sentence>();
            content = "";



        }


        /// <summary>
        /// Initializes a new instance of the <see cref="SentenceList"/> class.
        /// </summary>
        /// <param name="matchList">The match list.</param>
        /// <param name="wordPos">The word position.</param>
        /// <param name="numSentences">The number sentences.</param>
        /// <param name="avgLength">The average length.</param>
        /// <param name="sList">The s list.</param>
        /// <param name="content">The content.</param>
        public SentenceList(MatchCollection matchList, int wordPos, int numSentences, double avgLength, List<Sentence> sList, string content)
        {
            this.matchList = matchList;
            this.wordPos = wordPos;
            this.numSentences = numSentences;
            this.avgLength = avgLength;
            this.sList = sList;
            this.content = content;
        }


        /// <summary>
        /// Loads the file.
        /// </summary>
        /// <param name="input">The input.</param>
        public void loadFile(Text input)
        {
            content = input.OriginalString;
        }


        /// <summary>
        /// Processes the list.
        /// </summary>
        public void processList()
        {
            matchList = Regex.Matches(content, @"(\S.+?[.!?])(?=\s+|$)");
            var sentenceList = matchList.Cast<Match>().Select(match => match.Value).ToList();

            
            foreach (string s in sentenceList)
            {
                List<string> tempSentence = Utility.tokenizeWords(s);
                wordPos += tempSentence.Count;
                sentenceToAdd = new Sentence(s, wordPos);
                
                    sList.Add(sentenceToAdd);
                
            }

            numSentences = sList.Count;


            foreach (Sentence s in sList)
            {
                avgLength += s.NumWords;
            }
            avgLength = avgLength / numSentences;
            avgLength = Math.Round(avgLength, 2);
        }



        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            int sentenceNum = 1;
            string output = "Sentences Found in the Text: \n\n\n";
            foreach(Sentence s in sList)
            {
                output += ("Sentence " + sentenceNum + ": \n\n" +
                    s.ToString() + "\n");
                    sentenceNum++;
            }

            output += "There are " + numSentences + " sentences with an average length of " + Math.Round(avgLength,2) + " words.\n";

            return output;
        }
    }
}
