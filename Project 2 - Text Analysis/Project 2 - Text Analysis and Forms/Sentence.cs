﻿//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//	Project:		Project 2 - Text Analysis and Forms
//	File Name:		Sentence.cs
//	Description:    Stores information about each sentence and moderates the properties of the individual sentence.
//	Course:			CSCI 2210-001 - Data Structures
//	Author:			Brandi Campbell, campbellb1@goldmail.etsu.edu
//	Created:		Thursday, February 21, 2016
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////




using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Utils;


namespace TextAnalyzer
{
    /// <summary>
    /// This class defines what a sentence is within the Text Analysis program. It is a list of strings (words).
    /// It keeps track of the number of words, avg word length, where the starting word location 
    /// and the last word's location.  
    /// </summary>
    class Sentence
    {
        public string currentSentence; //content of current sentence
        public int numWords; //number of words in sentence
        public double avgWdLength;
        public int startWord; //position of the first word
        public int lastWord;  //position of last word



        /// <summary>
        /// Gets or sets the average length of the wd.
        /// </summary>
        /// <value>
        /// The average length of the wd.
        /// </value>
        public double AvgWdLength
        {
            get
            {
                return avgWdLength;
            }

            private set
            {
                avgWdLength = value;
            }
        }



        /// <summary>
        /// Gets or sets the number words.
        /// </summary>
        /// <value>
        /// The number words.
        /// </value>
        public int NumWords
        {
            get
            {
                return numWords;
            }

            private set
            {
                numWords = value;
            }
        }

        /// <summary>
        /// Gets or sets the start word.
        /// </summary>
        /// <value>
        /// The start word.
        /// </value>
        public int StartWord
        {
            get
            {
                return startWord;
            }

            set
            {
                startWord = value;
            }
        }


        /// <summary>
        /// Gets or sets the last word.
        /// </summary>
        /// <value>
        /// The last word.
        /// </value>
        public int LastWord
        {
            get
            {
                return lastWord;
            }

            set
            {
                lastWord = value;
            }
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="Sentence"/> class.
        /// </summary>
        /// <param name="content">The content.</param>
        /// <param name="firstPos">The first position.</param>
        public Sentence(string content,int firstPos)
        {
            currentSentence = content;

           
            startWord = firstPos;
            List<string> wordList = Utility.tokenizeWords(currentSentence);
            NumWords = wordList.Count;
            lastWord = firstPos + NumWords - 1;

            double sumLength = 0;

            foreach (string s in wordList)
            {
                sumLength += s.Length;
            }
            AvgWdLength = sumLength / NumWords;
            AvgWdLength = Math.Round(AvgWdLength, 2);






        }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            string output = currentSentence + "\n\n";
               

            return output;


        }
    }
}
