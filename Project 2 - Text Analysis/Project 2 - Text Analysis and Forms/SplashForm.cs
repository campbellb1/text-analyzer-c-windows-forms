﻿//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//	Project:		Project 2 - Text Analysis and Forms
//	File Name:		SplashForm.cs
//	Description:    Handles the "SplashScreen" and the speed at which the progress bar fills
//	Course:			CSCI 2210-001 - Data Structures
//	Author:			Brandi Campbell, campbellb1@goldmail.etsu.edu
//	Created:		Thursday, February 21, 2016
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////




using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TextAnalyzer
{

    /// <summary>
    /// Handles the "SplashScreen" and the speed at which the progress bar fills
    /// </summary>
    /// <seealso cref="System.Windows.Forms.Form" />
    public partial class SplashForm : Form
    {
        public SplashForm()
        {
            InitializeComponent();
        }



        /// <summary>
        /// Handles the Tick event of the splashTimer control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void splashTimer_Tick(object sender, EventArgs e)
        {
            
            loadBar.Increment(1);
            if(loadBar.Value == 100)
            {
                timer1.Stop();
            }
        }



        /// <summary>
        /// Handles the Click event of the splashLoadBar control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void splashLoadBar_Click(object sender, EventArgs e)
        {

        }
    }
}
