﻿//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//	Project:		Project 2 - Text Analysis and Forms
//	File Name:		MenuForm.cs
//	Description:    Serves as the functional Main Menu for this application. It allows the user to load a file and see all of the properties of the
//                  text contained within that file, so much as the program provides.
//	Course:			CSCI 2210-001 - Data Structures
//	Author:			Brandi Campbell, campbellb1@goldmail.etsu.edu
//	Created:		Monday, March 14 2016
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

using TextAnalyzer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Utils;

namespace TextAnalyzer
{



    /// <summary>
    /// This class represents the Main Window of the Text Analyzer program. From here, one can view all of the
    /// provided details regarding the loaded text (also allowing the user do perform said text loading).
    /// </summary>
    /// <seealso cref="System.Windows.Forms.Form" />
    public partial class MenuForm : Form
    {
        
        static bool fileLoaded = false; // Will be used to check if a file has been opened.
        static Text myText;                //Will hold the loaded text file.
        static string sFileName;        //Holds the filepath for the text file's location.
        static int paragraphIndex = 0; //Holds the index of the current paragraph
        static int sentenceIndex = 0;  //Holds the index of the current sentence
        User mainUser;                 //Holds user information inherited from another Form
        SentenceList newSentenceList;  //Manages the sentences
        ParagraphList newParagraphList;//Manages the paragraphs

        



        /// <summary>
        /// Initializes a new instance of the <see cref="MenuForm"/> class.
        /// </summary>
        /// <param name="inUser">The in user.</param>
        public MenuForm(User inUser)
        {
            List<Form> openForms = new List<Form>();

            foreach (Form f in Application.OpenForms)
                openForms.Add(f);

            foreach (Form f in openForms)
            {
                if (f.Name != "MenuForm")
                    f.Close();
            }

            mainUser = inUser; //sets the mainUser equal to the user definied in the Welcome Form
            InitializeComponent();
            this.ShowDialog();

            
        }



      
        /// <summary>
        /// Handles the Load event of the Form1 control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void welcomeForm_Load(object sender, EventArgs e)
        {

        }



        /// <summary>
        /// Handles the TextChanged event of the dataBox control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void dataBox_TextChanged(object sender, EventArgs e)
        {

        }




        /// <summary>
        /// Handles the Click event of the textTabPage control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void textTabPage_Click(object sender, EventArgs e)
        {

        }







        /// <summary>
        /// Handles the Click event of the loadAFileToolStripMenuItem control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void loadAFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog OpenDlg = new OpenFileDialog();             //open the file open dialog
            OpenDlg.InitialDirectory = System.IO.Path.GetFullPath(@"..\..\Text Files");
            OpenDlg.Filter = "text files|*.txt;*.text|all files|*.*";
            if (OpenDlg.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    sFileName = OpenDlg.FileName; //set the chosen file path to sFileName

                    fileLoaded = true; //make a note that a file has been loaded

                }
                catch (Exception x)
                {
                    MessageBox.Show("File was not loaded"); //if there is a problem loading the file, inform the user

                }
                myText = new Text(sFileName);

                //in the lines below, we replace \n with an Environment NewLine to make it work correctly with Windows Forms

                originalTxtBox.Text = myText.originalString.Replace("\n", Environment.NewLine); 
                tokensBox.Text = myText.printStringList().Replace("\n", Environment.NewLine);
                Words wordList = new Words(myText);

                dWordsBox.Text = wordList.ToString().Replace("\n", Environment.NewLine); //replace \n's with Env.NwLn for the Form to work

                newSentenceList = new SentenceList(); 
                newSentenceList.Content = myText.originalString; //set the contenct of newSentenceList to the text

                newSentenceList.loadFile(myText);
                newSentenceList.processList();


                //catch an error that occurs if a file cannot be displayed in Forms properly
                try
                {
                    sentencesBox.Text = newSentenceList.sList[sentenceIndex].ToString().Replace("\n", Environment.NewLine);


                    newParagraphList = new ParagraphList(myText);
                    paragraphBox.Text = newParagraphList.PList[paragraphIndex].ToString().Replace("\n", Environment.NewLine);
                }
                catch (Exception noDisplay)
                {
                    MessageBox.Show("The Text could not be displayed!");
                }

                //setup the sentence tab number box
                sentenceNumBox.Minimum = 1;
                sentenceNumBox.Maximum = newSentenceList.SList.Count;

                //setup the paragraph tab number box
                paragraphListBox.Minimum = 1;
                paragraphListBox.Maximum = newParagraphList.PList.Count;
                

                //populate the sentence tab's content
                sentenceNumWordsBox.Text = newSentenceList.sList[sentenceIndex].numWords.ToString();
                sentenceAvgWordsBox.Text = newSentenceList.sList[sentenceIndex].AvgWdLength.ToString();
                avgWordsBox.Text = newParagraphList.PList[paragraphIndex].avgSentenceLength.ToString();
                numSentencesBox.Text = newParagraphList.PList[paragraphIndex].NumSentences.ToString();
                pgphNumWordsBox.Text = newParagraphList.PList[paragraphIndex].numWords.ToString();

                //populate the bottom of the form with information.
                fileToLoad.Text = sFileName.Substring(sFileName.LastIndexOf("\\"));
                ttlNumTkns.Text = myText.stringList.Count.ToString();
                ttlNumDWords.Text = wordList.WordList.Count.ToString();
                ttlNumSents.Text = newParagraphList.NumSentences.ToString();
                ttlNumPgphs.Text = newParagraphList.NumParagraphs.ToString();
            }
        }






        /// <summary>
        /// Handles the Click event of the tokenPage control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void tokenPage_Click(object sender, EventArgs e)
        {

        }





        /// <summary>
        /// Handles the TextChanged event of the tokensBox control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void tokensBox_TextChanged(object sender, EventArgs e)
        {

        }







        /// <summary>
        /// Handles the 1 event of the dWordsBox_TextChanged control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void dWordsBox_TextChanged_1(object sender, EventArgs e)
        {

        }







        /// <summary>
        /// Handles the TextChanged event of the originalTxtBox control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void originalTxtBox_TextChanged(object sender, EventArgs e)
        {

        }







        /// <summary>
        /// Handles the TextChanged event of the sentencesBox control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void sentencesBox_TextChanged(object sender, EventArgs e)
        {

        }






        /// <summary>
        /// Handles the Click event of the paragraphPage control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void paragraphPage_Click(object sender, EventArgs e)
        {

        }






        /// <summary>
        /// Handles the TextChanged event of the paragraphBox control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void paragraphBox_TextChanged(object sender, EventArgs e)
        {

        }






        /// <summary>
        /// Handles the Click event of the aboutToolStripMenuItem control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutForm myAbout = new AboutForm();
            myAbout.ShowDialog();
        }



        /// <summary>
        /// Handles the Click event of the sentenceNextBtn control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void sentenceNextBtn_Click(object sender, EventArgs e)
        {
           
               
            

        }


        /// <summary>
        /// Handles the Click event of the sentencePrevBtn control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void sentencePrevBtn_Click(object sender, EventArgs e)
        {

           

        }



        /// <summary>
        /// Handles the 1 event of the sentenceNextBtn_Click control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void sentenceNextBtn_Click_1(object sender, EventArgs e)
        {
            try
            {
                if (sentenceIndex != newSentenceList.sList.Count - 1)
                {
                    sentenceIndex++;
                    sentencesBox.Text = newSentenceList.sList[sentenceIndex].ToString().Replace("\n", Environment.NewLine);
                    sentenceNumBox.Value = (sentenceIndex + 1);
                    sentenceNumWordsBox.Text = newSentenceList.sList[sentenceIndex].numWords.ToString();
                    sentenceAvgWordsBox.Text = newSentenceList.sList[sentenceIndex].AvgWdLength.ToString();
                }
            }
            catch (Exception outOfBounds)
            {

            }

        }



        /// <summary>
        /// Handles the Click event of the paragraphNextBtn control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void paragraphNextBtn_Click(object sender, EventArgs e)
        {
            try
            {
                if (paragraphIndex != newParagraphList.PList.Count - 1)
                {
                    paragraphIndex++;
                    paragraphBox.Text = newParagraphList.PList[paragraphIndex].ToString().Replace("\n", Environment.NewLine);
                    paragraphListBox.Value = paragraphIndex + 1;
                    avgWordsBox.Text = newParagraphList.PList[paragraphIndex].avgSentenceLength.ToString();
                    numSentencesBox.Text = newParagraphList.PList[paragraphIndex].NumSentences.ToString();
                    pgphNumWordsBox.Text = newParagraphList.PList[paragraphIndex].numWords.ToString();

                }
            }
            catch(Exception outOfBounds)
            {

            }
            
        }



        /// <summary>
        /// Handles the Click event of the paragraphPrevBtn control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void paragraphPrevBtn_Click(object sender, EventArgs e)
        {
            if (paragraphIndex != 0)
            {
                paragraphIndex--;
                paragraphBox.Text = newParagraphList.PList[paragraphIndex].ToString().Replace("\n", Environment.NewLine);
                paragraphListBox.Value = paragraphIndex + 1;
                avgWordsBox.Text = newParagraphList.PList[paragraphIndex].avgSentenceLength.ToString();
                numSentencesBox.Text = newParagraphList.PList[paragraphIndex].NumSentences.ToString();
                pgphNumWordsBox.Text = newParagraphList.PList[paragraphIndex].numWords.ToString();

            }
        }



        /// <summary>
        /// Handles the 1 event of the sentencePrevBtn_Click control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void sentencePrevBtn_Click_1(object sender, EventArgs e)
        {
            if (sentenceIndex != 0)
            {
                sentenceIndex--;
                sentencesBox.Text = newSentenceList.sList[sentenceIndex].ToString().Replace("\n", Environment.NewLine);
                sentenceNumBox.Value = (sentenceIndex + 1);
                sentenceNumWordsBox.Text = newSentenceList.sList[sentenceIndex].numWords.ToString();
                sentenceAvgWordsBox.Text = newSentenceList.sList[sentenceIndex].AvgWdLength.ToString();


            }
        }

        

        /// <summary>
        /// Handles the Click event of the sentenceLabel control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void sentenceLabel_Click(object sender, EventArgs e)
        {

        }

        
        /// <summary>
        /// Handles the Click event of the sentAvgWordLengthLabel control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void sentAvgWordLengthLabel_Click(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Handles the 1 event of the aboutToolStripMenuItem_Click control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void aboutToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            AboutForm help = new AboutForm();
            help.ShowDialog();
        }


        /// <summary>
        /// Handles the 2 event of the sentenceNumWordsBox TextChanged control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void sentenceNumWordsBox_TextChanged_2(object sender, EventArgs e)
        {

        }



        /// <summary>
        /// Handles the Click event of the paraNumSentLabel control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void paraNumSentLabel_Click(object sender, EventArgs e)
        {

        }



        /// <summary>
        /// Handles the Click event of the paraAvgWrdLbl control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void paraAvgWrdLbl_Click(object sender, EventArgs e)
        {

        }



        /// <summary>
        /// Handles the TextChanged event of the numSentencesBox control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void numSentencesBox_TextChanged(object sender, EventArgs e)
        {

        }



        /// <summary>
        /// Handles the Click event of the fileLabel control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void fileLabel_Click(object sender, EventArgs e)
        {

        }



        /// <summary>
        /// Handles the Click event of the fileToLoad control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void fileToLoad_Click(object sender, EventArgs e)
        {

        }




        /// <summary>
        /// Handles the Click event of the tokensLabel control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void tokensLabel_Click(object sender, EventArgs e)
        {

        }



        /// <summary>
        /// Handles the Click event of the ttlNumSents control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void ttlNumSents_Click(object sender, EventArgs e)
        {

        }



        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Form.FormClosing" /> event.
        /// </summary>
        /// <param name="e">A <see cref="T:System.Windows.Forms.FormClosingEventArgs" /> that contains the event data.</param>
        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            base.OnFormClosing(e);

            if (e.CloseReason == CloseReason.WindowsShutDown) return;

            // Confirm user wants to close
            mainUser.userName.parseName();
            switch (MessageBox.Show(this, "Thank you for using the Text Analyzer " + mainUser.userName.RestOfName + "\nAre you sure you want to close?", "Closing", MessageBoxButtons.YesNo))
            {
                case DialogResult.No:
                    e.Cancel = true;
                    break;
                default:
                    break;
            }
        }



        /// <summary>
        /// Handles the Click event of the exitProgramToolStripMenuItem control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void exitProgramToolStripMenuItem_Click(object sender, EventArgs e)
        {
            switch (MessageBox.Show(this, "Thank you for using the Text Analyzer " + mainUser.userName.RestOfName + "\nAre you sure you want to close?", "Closing", MessageBoxButtons.YesNo))
            {
                case DialogResult.No:
                   
                    break;
                default:
                    Environment.Exit(0);
                    break;
            }
        }



        /// <summary>
        /// Handles the Click event of the fileToolStripMenuItem control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void fileToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Handles the ValueChanged event of the sentenceNumBox control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void sentenceNumBox_ValueChanged(object sender, EventArgs e)
        {

            try
            {
                sentencesBox.Text = newSentenceList.sList[Decimal.ToInt32(sentenceNumBox.Value - 1)].ToString().Replace("\n", Environment.NewLine);
                sentenceNumWordsBox.Text = newSentenceList.sList[Decimal.ToInt32(sentenceNumBox.Value - 1)].numWords.ToString();
                sentenceAvgWordsBox.Text = newSentenceList.sList[Decimal.ToInt32(sentenceNumBox.Value - 1)].AvgWdLength.ToString();
            }
            catch (Exception fileNotLoaded)
            {

            }
        }



        /// <summary>
        /// Handles the ValueChanged event of the paragraphListBox control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void paragraphListBox_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                paragraphBox.Text = newParagraphList.PList[Decimal.ToInt32(paragraphListBox.Value - 1)].ToString().Replace("\n", Environment.NewLine);
                paraNumWordLabel.Text = newParagraphList.PList[Decimal.ToInt32(paragraphListBox.Value - 1)].numWords.ToString();
                avgWordsBox.Text = newParagraphList.PList[Decimal.ToInt32(paragraphListBox.Value - 1)].avgSentenceLength.ToString();
            }
            catch (Exception fileNotLoaded)
            {

            }

            }



        /// <summary>
        /// Handles the Click event of the ttlNumDWords control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void ttlNumDWords_Click(object sender, EventArgs e)
        {

        }
    }

    
    
}
