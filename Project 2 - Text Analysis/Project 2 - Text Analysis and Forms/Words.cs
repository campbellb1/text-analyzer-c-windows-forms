﻿//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//	Project:		Project 2 - Text Analysis and Forms
//	File Name:		Words.cs
//	Description:    Holds a list of Distinct Word items. This class's constructor cleans up the tokenized string and can display a list of the 
//                  words and the number of occurrences that each word presents.
//	Course:			CSCI 2210-001 - Data Structures
//	Author:			Brandi Campbell, campbellb1@goldmail.etsu.edu
//	Created:		Thursday, February 21, 2016
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Utils;


namespace TextAnalyzer
{

    /// <summary>
    /// This class holds a list of Distinct Word items. This class's constructor cleans up the tokenized string
    /// and can display a list of the words and the number of occurrences that each word presents.
    /// </summary>
    class Words
    {
        List<DistinctWord> wordList = new List<DistinctWord>();
        readonly int wordCount;

        /// <summary>
        /// Gets or sets the word list.
        /// </summary>
        /// <value>
        /// The word list.
        /// </value>
        public List<DistinctWord> WordList
        {
            get
            {
                return wordList;
            }

            private set
            {
                wordList = value;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Words"/> class.
        /// </summary>
        public Words()
        {
            wordCount = 0;
            
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Words"/> class.
        /// </summary>
        /// <param name="text">The text.</param>
        public Words(Text text)
        {
            //create a new list from the Text file passed in
            List<string> stringList = new List<string>();
            text.mkTknText();
            stringList = text.returnTknString();
            Regex wordOnly = new Regex("(\\w+'*\\w*)", RegexOptions.IgnoreCase);



            //get only unique words from the list
            var uniqueWords = stringList.Distinct().ToList();
            stringList = uniqueWords;


            foreach (string str in stringList)
            {
                str.ToLower();                                         //Make each word lowercase

                //add each word to the Word List as each one is matched.
                if (wordOnly.Match(str).Success)
                {
                    WordList.Add(new DistinctWord(str));
                 
                }
                
            }

            foreach(DistinctWord distinct in WordList)
            {
                distinct.countInstances(text.OriginalString);
            }

            //The following code alphabetizes the WordList.
            DistinctWord tempWord;
            wordCount = WordList.Count; //set the count of the Word list to reflect the Class's wordCount.
            int size = WordList.Count - 1;
            for(int n = size; n > 0; n-- )
            {
                int maxWord = 0; //reset the "highest valued" word

                for (int x = 1; x < n; x++)
                {
                    if (WordList[maxWord].CompareTo(WordList[x]) < 0)
                    {
                        maxWord = x;
                        
                    }

                    if (maxWord != n) //if the "highest valued" word is not in the last position, swap.
                    {
                        tempWord = WordList[n];
                        WordList[n] = WordList[maxWord];
                        WordList[maxWord] = tempWord;
                    }

                }
               

            }

           
        }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        override public string ToString()
        {
            string output = String.Format("{0,-6} {1,15}\n", "Words", "Occurrence");

            
            foreach (DistinctWord d in WordList)
            {
                if (d.Occurrences !=0)
                output += d.ToString();
              
            }

            return output;
        }

    }
}
