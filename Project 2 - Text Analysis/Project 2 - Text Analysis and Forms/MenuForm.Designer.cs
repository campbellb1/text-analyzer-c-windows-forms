﻿using System.Windows.Forms;

namespace TextAnalyzer
{
    partial class MenuForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MenuForm));
            this.mainTabMenu = new System.Windows.Forms.TabControl();
            this.originalText = new System.Windows.Forms.TabPage();
            this.originalTxtBox = new System.Windows.Forms.TextBox();
            this.tokenPage = new System.Windows.Forms.TabPage();
            this.tokensBox = new System.Windows.Forms.TextBox();
            this.dWordsPage = new System.Windows.Forms.TabPage();
            this.dWordsBox = new System.Windows.Forms.TextBox();
            this.sentencesPage = new System.Windows.Forms.TabPage();
            this.sentenceNumBox = new System.Windows.Forms.NumericUpDown();
            this.sentAvgWordLengthLabel = new System.Windows.Forms.Label();
            this.sentNumWordsLabel = new System.Windows.Forms.Label();
            this.sentenceAvgWordsBox = new System.Windows.Forms.TextBox();
            this.sentenceNumWordsBox = new System.Windows.Forms.TextBox();
            this.dsplSentNumLabel = new System.Windows.Forms.Label();
            this.sentencePrevBtn = new System.Windows.Forms.Button();
            this.sentenceNextBtn = new System.Windows.Forms.Button();
            this.sentencesBox = new System.Windows.Forms.TextBox();
            this.paragraphPage = new System.Windows.Forms.TabPage();
            this.paragraphListBox = new System.Windows.Forms.NumericUpDown();
            this.pgphNumWordsBox = new System.Windows.Forms.TextBox();
            this.paraNumWordLabel = new System.Windows.Forms.Label();
            this.paraAvgWrdLabel = new System.Windows.Forms.Label();
            this.paraNumSentLabel = new System.Windows.Forms.Label();
            this.avgWordsBox = new System.Windows.Forms.TextBox();
            this.numSentencesBox = new System.Windows.Forms.TextBox();
            this.dsplParaNumLabel = new System.Windows.Forms.Label();
            this.paragraphPrevBtn = new System.Windows.Forms.Button();
            this.paragraphNextBtn = new System.Windows.Forms.Button();
            this.paragraphBox = new System.Windows.Forms.TextBox();
            this.topMenuStrip = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadAFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitProgramToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fileLabel = new System.Windows.Forms.Label();
            this.fileToLoad = new System.Windows.Forms.Label();
            this.tokensLabel = new System.Windows.Forms.Label();
            this.dWordLabel = new System.Windows.Forms.Label();
            this.sentencesLabel = new System.Windows.Forms.Label();
            this.paragraphsLabel = new System.Windows.Forms.Label();
            this.ttlNumTkns = new System.Windows.Forms.Label();
            this.ttlNumDWords = new System.Windows.Forms.Label();
            this.ttlNumSents = new System.Windows.Forms.Label();
            this.ttlNumPgphs = new System.Windows.Forms.Label();
            this.mainTabMenu.SuspendLayout();
            this.originalText.SuspendLayout();
            this.tokenPage.SuspendLayout();
            this.dWordsPage.SuspendLayout();
            this.sentencesPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sentenceNumBox)).BeginInit();
            this.paragraphPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.paragraphListBox)).BeginInit();
            this.topMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainTabMenu
            // 
            this.mainTabMenu.Controls.Add(this.originalText);
            this.mainTabMenu.Controls.Add(this.tokenPage);
            this.mainTabMenu.Controls.Add(this.dWordsPage);
            this.mainTabMenu.Controls.Add(this.sentencesPage);
            this.mainTabMenu.Controls.Add(this.paragraphPage);
            this.mainTabMenu.Location = new System.Drawing.Point(12, 27);
            this.mainTabMenu.Name = "mainTabMenu";
            this.mainTabMenu.SelectedIndex = 0;
            this.mainTabMenu.Size = new System.Drawing.Size(619, 387);
            this.mainTabMenu.TabIndex = 1;
            // 
            // originalText
            // 
            this.originalText.Controls.Add(this.originalTxtBox);
            this.originalText.Location = new System.Drawing.Point(4, 22);
            this.originalText.Name = "originalText";
            this.originalText.Padding = new System.Windows.Forms.Padding(3);
            this.originalText.Size = new System.Drawing.Size(611, 361);
            this.originalText.TabIndex = 0;
            this.originalText.Text = "Original Text";
            this.originalText.UseVisualStyleBackColor = true;
            this.originalText.Click += new System.EventHandler(this.textTabPage_Click);
            // 
            // originalTxtBox
            // 
            this.originalTxtBox.AcceptsReturn = true;
            this.originalTxtBox.AcceptsTab = true;
            this.originalTxtBox.Location = new System.Drawing.Point(3, 3);
            this.originalTxtBox.Multiline = true;
            this.originalTxtBox.Name = "originalTxtBox";
            this.originalTxtBox.ReadOnly = true;
            this.originalTxtBox.Size = new System.Drawing.Size(608, 286);
            this.originalTxtBox.TabIndex = 0;
            this.originalTxtBox.TextChanged += new System.EventHandler(this.originalTxtBox_TextChanged);
            // 
            // tokenPage
            // 
            this.tokenPage.Controls.Add(this.tokensBox);
            this.tokenPage.Location = new System.Drawing.Point(4, 22);
            this.tokenPage.Name = "tokenPage";
            this.tokenPage.Padding = new System.Windows.Forms.Padding(3);
            this.tokenPage.Size = new System.Drawing.Size(611, 361);
            this.tokenPage.TabIndex = 1;
            this.tokenPage.Text = "Tokens";
            this.tokenPage.UseVisualStyleBackColor = true;
            this.tokenPage.Click += new System.EventHandler(this.tokenPage_Click);
            // 
            // tokensBox
            // 
            this.tokensBox.AcceptsReturn = true;
            this.tokensBox.AcceptsTab = true;
            this.tokensBox.Location = new System.Drawing.Point(2, 3);
            this.tokensBox.Multiline = true;
            this.tokensBox.Name = "tokensBox";
            this.tokensBox.ReadOnly = true;
            this.tokensBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tokensBox.Size = new System.Drawing.Size(601, 325);
            this.tokensBox.TabIndex = 1;
            this.tokensBox.TextChanged += new System.EventHandler(this.tokensBox_TextChanged);
            // 
            // dWordsPage
            // 
            this.dWordsPage.Controls.Add(this.dWordsBox);
            this.dWordsPage.Location = new System.Drawing.Point(4, 22);
            this.dWordsPage.Name = "dWordsPage";
            this.dWordsPage.Size = new System.Drawing.Size(611, 361);
            this.dWordsPage.TabIndex = 2;
            this.dWordsPage.Text = "Distinct Words";
            this.dWordsPage.UseVisualStyleBackColor = true;
            // 
            // dWordsBox
            // 
            this.dWordsBox.AcceptsReturn = true;
            this.dWordsBox.AcceptsTab = true;
            this.dWordsBox.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dWordsBox.Location = new System.Drawing.Point(2, 3);
            this.dWordsBox.Multiline = true;
            this.dWordsBox.Name = "dWordsBox";
            this.dWordsBox.ReadOnly = true;
            this.dWordsBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dWordsBox.Size = new System.Drawing.Size(601, 325);
            this.dWordsBox.TabIndex = 2;
            this.dWordsBox.TextChanged += new System.EventHandler(this.dWordsBox_TextChanged_1);
            // 
            // sentencesPage
            // 
            this.sentencesPage.Controls.Add(this.sentenceNumBox);
            this.sentencesPage.Controls.Add(this.sentAvgWordLengthLabel);
            this.sentencesPage.Controls.Add(this.sentNumWordsLabel);
            this.sentencesPage.Controls.Add(this.sentenceAvgWordsBox);
            this.sentencesPage.Controls.Add(this.sentenceNumWordsBox);
            this.sentencesPage.Controls.Add(this.dsplSentNumLabel);
            this.sentencesPage.Controls.Add(this.sentencePrevBtn);
            this.sentencesPage.Controls.Add(this.sentenceNextBtn);
            this.sentencesPage.Controls.Add(this.sentencesBox);
            this.sentencesPage.Location = new System.Drawing.Point(4, 22);
            this.sentencesPage.Name = "sentencesPage";
            this.sentencesPage.Padding = new System.Windows.Forms.Padding(3);
            this.sentencesPage.Size = new System.Drawing.Size(611, 361);
            this.sentencesPage.TabIndex = 3;
            this.sentencesPage.Text = "Sentences";
            this.sentencesPage.UseVisualStyleBackColor = true;
            // 
            // sentenceNumBox
            // 
            this.sentenceNumBox.Location = new System.Drawing.Point(304, 8);
            this.sentenceNumBox.Name = "sentenceNumBox";
            this.sentenceNumBox.Size = new System.Drawing.Size(56, 20);
            this.sentenceNumBox.TabIndex = 15;
            this.sentenceNumBox.ValueChanged += new System.EventHandler(this.sentenceNumBox_ValueChanged);
            // 
            // sentAvgWordLengthLabel
            // 
            this.sentAvgWordLengthLabel.AutoSize = true;
            this.sentAvgWordLengthLabel.Location = new System.Drawing.Point(186, 306);
            this.sentAvgWordLengthLabel.Name = "sentAvgWordLengthLabel";
            this.sentAvgWordLengthLabel.Size = new System.Drawing.Size(112, 13);
            this.sentAvgWordLengthLabel.TabIndex = 14;
            this.sentAvgWordLengthLabel.Text = "Average Word Length";
            this.sentAvgWordLengthLabel.Click += new System.EventHandler(this.sentAvgWordLengthLabel_Click);
            // 
            // sentNumWordsLabel
            // 
            this.sentNumWordsLabel.AutoSize = true;
            this.sentNumWordsLabel.Location = new System.Drawing.Point(186, 277);
            this.sentNumWordsLabel.Name = "sentNumWordsLabel";
            this.sentNumWordsLabel.Size = new System.Drawing.Size(90, 13);
            this.sentNumWordsLabel.TabIndex = 13;
            this.sentNumWordsLabel.Text = "Number of Words";
            this.sentNumWordsLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // sentenceAvgWordsBox
            // 
            this.sentenceAvgWordsBox.Location = new System.Drawing.Point(324, 303);
            this.sentenceAvgWordsBox.Name = "sentenceAvgWordsBox";
            this.sentenceAvgWordsBox.ReadOnly = true;
            this.sentenceAvgWordsBox.Size = new System.Drawing.Size(100, 20);
            this.sentenceAvgWordsBox.TabIndex = 12;
            // 
            // sentenceNumWordsBox
            // 
            this.sentenceNumWordsBox.Location = new System.Drawing.Point(324, 274);
            this.sentenceNumWordsBox.Name = "sentenceNumWordsBox";
            this.sentenceNumWordsBox.ReadOnly = true;
            this.sentenceNumWordsBox.Size = new System.Drawing.Size(100, 20);
            this.sentenceNumWordsBox.TabIndex = 11;
            this.sentenceNumWordsBox.TextChanged += new System.EventHandler(this.sentenceNumWordsBox_TextChanged_2);
            // 
            // dsplSentNumLabel
            // 
            this.dsplSentNumLabel.AutoSize = true;
            this.dsplSentNumLabel.Location = new System.Drawing.Point(161, 10);
            this.dsplSentNumLabel.Name = "dsplSentNumLabel";
            this.dsplSentNumLabel.Size = new System.Drawing.Size(133, 13);
            this.dsplSentNumLabel.TabIndex = 8;
            this.dsplSentNumLabel.Text = "Display Sentence Number:";
            this.dsplSentNumLabel.Click += new System.EventHandler(this.sentenceLabel_Click);
            // 
            // sentencePrevBtn
            // 
            this.sentencePrevBtn.Location = new System.Drawing.Point(219, 329);
            this.sentencePrevBtn.Name = "sentencePrevBtn";
            this.sentencePrevBtn.Size = new System.Drawing.Size(75, 23);
            this.sentencePrevBtn.TabIndex = 7;
            this.sentencePrevBtn.Text = "<--- Prev.";
            this.sentencePrevBtn.UseVisualStyleBackColor = true;
            this.sentencePrevBtn.Click += new System.EventHandler(this.sentencePrevBtn_Click_1);
            // 
            // sentenceNextBtn
            // 
            this.sentenceNextBtn.Location = new System.Drawing.Point(324, 329);
            this.sentenceNextBtn.Name = "sentenceNextBtn";
            this.sentenceNextBtn.Size = new System.Drawing.Size(75, 23);
            this.sentenceNextBtn.TabIndex = 6;
            this.sentenceNextBtn.Text = "Next --->";
            this.sentenceNextBtn.UseVisualStyleBackColor = true;
            this.sentenceNextBtn.Click += new System.EventHandler(this.sentenceNextBtn_Click_1);
            // 
            // sentencesBox
            // 
            this.sentencesBox.AcceptsReturn = true;
            this.sentencesBox.AcceptsTab = true;
            this.sentencesBox.Location = new System.Drawing.Point(3, 47);
            this.sentencesBox.Multiline = true;
            this.sentencesBox.Name = "sentencesBox";
            this.sentencesBox.ReadOnly = true;
            this.sentencesBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.sentencesBox.Size = new System.Drawing.Size(601, 210);
            this.sentencesBox.TabIndex = 3;
            this.sentencesBox.TextChanged += new System.EventHandler(this.sentencesBox_TextChanged);
            // 
            // paragraphPage
            // 
            this.paragraphPage.Controls.Add(this.paragraphListBox);
            this.paragraphPage.Controls.Add(this.pgphNumWordsBox);
            this.paragraphPage.Controls.Add(this.paraNumWordLabel);
            this.paragraphPage.Controls.Add(this.paraAvgWrdLabel);
            this.paragraphPage.Controls.Add(this.paraNumSentLabel);
            this.paragraphPage.Controls.Add(this.avgWordsBox);
            this.paragraphPage.Controls.Add(this.numSentencesBox);
            this.paragraphPage.Controls.Add(this.dsplParaNumLabel);
            this.paragraphPage.Controls.Add(this.paragraphPrevBtn);
            this.paragraphPage.Controls.Add(this.paragraphNextBtn);
            this.paragraphPage.Controls.Add(this.paragraphBox);
            this.paragraphPage.Location = new System.Drawing.Point(4, 22);
            this.paragraphPage.Name = "paragraphPage";
            this.paragraphPage.Padding = new System.Windows.Forms.Padding(3);
            this.paragraphPage.Size = new System.Drawing.Size(611, 361);
            this.paragraphPage.TabIndex = 4;
            this.paragraphPage.Text = "Paragraphs";
            this.paragraphPage.UseVisualStyleBackColor = true;
            this.paragraphPage.Click += new System.EventHandler(this.paragraphPage_Click);
            // 
            // paragraphListBox
            // 
            this.paragraphListBox.Location = new System.Drawing.Point(303, 8);
            this.paragraphListBox.Name = "paragraphListBox";
            this.paragraphListBox.Size = new System.Drawing.Size(56, 20);
            this.paragraphListBox.TabIndex = 21;
            this.paragraphListBox.ValueChanged += new System.EventHandler(this.paragraphListBox_ValueChanged);
            // 
            // pgphNumWordsBox
            // 
            this.pgphNumWordsBox.Location = new System.Drawing.Point(324, 287);
            this.pgphNumWordsBox.Name = "pgphNumWordsBox";
            this.pgphNumWordsBox.ReadOnly = true;
            this.pgphNumWordsBox.Size = new System.Drawing.Size(100, 20);
            this.pgphNumWordsBox.TabIndex = 20;
            // 
            // paraNumWordLabel
            // 
            this.paraNumWordLabel.AutoSize = true;
            this.paraNumWordLabel.Location = new System.Drawing.Point(169, 290);
            this.paraNumWordLabel.Name = "paraNumWordLabel";
            this.paraNumWordLabel.Size = new System.Drawing.Size(90, 13);
            this.paraNumWordLabel.TabIndex = 19;
            this.paraNumWordLabel.Text = "Number of Words";
            // 
            // paraAvgWrdLabel
            // 
            this.paraAvgWrdLabel.AutoSize = true;
            this.paraAvgWrdLabel.Location = new System.Drawing.Point(169, 310);
            this.paraAvgWrdLabel.Name = "paraAvgWrdLabel";
            this.paraAvgWrdLabel.Size = new System.Drawing.Size(149, 13);
            this.paraAvgWrdLabel.TabIndex = 18;
            this.paraAvgWrdLabel.Text = "Average Words Per Sentence";
            this.paraAvgWrdLabel.Click += new System.EventHandler(this.paraAvgWrdLbl_Click);
            // 
            // paraNumSentLabel
            // 
            this.paraNumSentLabel.AutoSize = true;
            this.paraNumSentLabel.Location = new System.Drawing.Point(169, 267);
            this.paraNumSentLabel.Name = "paraNumSentLabel";
            this.paraNumSentLabel.Size = new System.Drawing.Size(110, 13);
            this.paraNumSentLabel.TabIndex = 17;
            this.paraNumSentLabel.Text = "Number of Sentences";
            this.paraNumSentLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.paraNumSentLabel.Click += new System.EventHandler(this.paraNumSentLabel_Click);
            // 
            // avgWordsBox
            // 
            this.avgWordsBox.Location = new System.Drawing.Point(324, 310);
            this.avgWordsBox.Name = "avgWordsBox";
            this.avgWordsBox.ReadOnly = true;
            this.avgWordsBox.Size = new System.Drawing.Size(100, 20);
            this.avgWordsBox.TabIndex = 16;
            // 
            // numSentencesBox
            // 
            this.numSentencesBox.Location = new System.Drawing.Point(324, 264);
            this.numSentencesBox.Name = "numSentencesBox";
            this.numSentencesBox.ReadOnly = true;
            this.numSentencesBox.Size = new System.Drawing.Size(100, 20);
            this.numSentencesBox.TabIndex = 15;
            this.numSentencesBox.TextChanged += new System.EventHandler(this.numSentencesBox_TextChanged);
            // 
            // dsplParaNumLabel
            // 
            this.dsplParaNumLabel.AutoSize = true;
            this.dsplParaNumLabel.Location = new System.Drawing.Point(161, 10);
            this.dsplParaNumLabel.Name = "dsplParaNumLabel";
            this.dsplParaNumLabel.Size = new System.Drawing.Size(136, 13);
            this.dsplParaNumLabel.TabIndex = 11;
            this.dsplParaNumLabel.Text = "Display Paragraph Number:";
            // 
            // paragraphPrevBtn
            // 
            this.paragraphPrevBtn.Location = new System.Drawing.Point(219, 329);
            this.paragraphPrevBtn.Name = "paragraphPrevBtn";
            this.paragraphPrevBtn.Size = new System.Drawing.Size(75, 23);
            this.paragraphPrevBtn.TabIndex = 7;
            this.paragraphPrevBtn.Text = "<--- Prev.";
            this.paragraphPrevBtn.UseVisualStyleBackColor = true;
            this.paragraphPrevBtn.Click += new System.EventHandler(this.paragraphPrevBtn_Click);
            // 
            // paragraphNextBtn
            // 
            this.paragraphNextBtn.Location = new System.Drawing.Point(324, 329);
            this.paragraphNextBtn.Name = "paragraphNextBtn";
            this.paragraphNextBtn.Size = new System.Drawing.Size(75, 23);
            this.paragraphNextBtn.TabIndex = 6;
            this.paragraphNextBtn.Text = "Next --->";
            this.paragraphNextBtn.UseVisualStyleBackColor = true;
            this.paragraphNextBtn.Click += new System.EventHandler(this.paragraphNextBtn_Click);
            // 
            // paragraphBox
            // 
            this.paragraphBox.AcceptsReturn = true;
            this.paragraphBox.AcceptsTab = true;
            this.paragraphBox.Location = new System.Drawing.Point(3, 47);
            this.paragraphBox.Multiline = true;
            this.paragraphBox.Name = "paragraphBox";
            this.paragraphBox.ReadOnly = true;
            this.paragraphBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.paragraphBox.Size = new System.Drawing.Size(601, 210);
            this.paragraphBox.TabIndex = 4;
            this.paragraphBox.TextChanged += new System.EventHandler(this.paragraphBox_TextChanged);
            // 
            // topMenuStrip
            // 
            this.topMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.topMenuStrip.Location = new System.Drawing.Point(0, 0);
            this.topMenuStrip.Name = "topMenuStrip";
            this.topMenuStrip.Size = new System.Drawing.Size(654, 24);
            this.topMenuStrip.TabIndex = 3;
            this.topMenuStrip.Text = "menuStrip2";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadAFileToolStripMenuItem,
            this.exitProgramToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F)));
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            this.fileToolStripMenuItem.Click += new System.EventHandler(this.fileToolStripMenuItem_Click);
            // 
            // loadAFileToolStripMenuItem
            // 
            this.loadAFileToolStripMenuItem.Name = "loadAFileToolStripMenuItem";
            this.loadAFileToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.L)));
            this.loadAFileToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
            this.loadAFileToolStripMenuItem.Text = "Load a File";
            this.loadAFileToolStripMenuItem.Click += new System.EventHandler(this.loadAFileToolStripMenuItem_Click);
            // 
            // exitProgramToolStripMenuItem
            // 
            this.exitProgramToolStripMenuItem.Name = "exitProgramToolStripMenuItem";
            this.exitProgramToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.E)));
            this.exitProgramToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
            this.exitProgramToolStripMenuItem.Text = "Exit Program";
            this.exitProgramToolStripMenuItem.Click += new System.EventHandler(this.exitProgramToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.H)));
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.A)));
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click_1);
            // 
            // fileLabel
            // 
            this.fileLabel.AutoSize = true;
            this.fileLabel.Location = new System.Drawing.Point(19, 417);
            this.fileLabel.Name = "fileLabel";
            this.fileLabel.Size = new System.Drawing.Size(26, 13);
            this.fileLabel.TabIndex = 4;
            this.fileLabel.Text = "File:";
            this.fileLabel.Click += new System.EventHandler(this.fileLabel_Click);
            // 
            // fileToLoad
            // 
            this.fileToLoad.AutoSize = true;
            this.fileToLoad.Location = new System.Drawing.Point(85, 417);
            this.fileToLoad.Name = "fileToLoad";
            this.fileToLoad.Size = new System.Drawing.Size(49, 13);
            this.fileToLoad.TabIndex = 5;
            this.fileToLoad.Text = "filetoload";
            this.fileToLoad.Click += new System.EventHandler(this.fileToLoad_Click);
            // 
            // tokensLabel
            // 
            this.tokensLabel.AutoSize = true;
            this.tokensLabel.Location = new System.Drawing.Point(270, 417);
            this.tokensLabel.Name = "tokensLabel";
            this.tokensLabel.Size = new System.Drawing.Size(46, 13);
            this.tokensLabel.TabIndex = 6;
            this.tokensLabel.Text = "Tokens:";
            this.tokensLabel.Click += new System.EventHandler(this.tokensLabel_Click);
            // 
            // dWordLabel
            // 
            this.dWordLabel.AutoSize = true;
            this.dWordLabel.Location = new System.Drawing.Point(346, 417);
            this.dWordLabel.Name = "dWordLabel";
            this.dWordLabel.Size = new System.Drawing.Size(79, 13);
            this.dWordLabel.TabIndex = 5;
            this.dWordLabel.Text = "Distinct Words:";
            // 
            // sentencesLabel
            // 
            this.sentencesLabel.AutoSize = true;
            this.sentencesLabel.Location = new System.Drawing.Point(455, 417);
            this.sentencesLabel.Name = "sentencesLabel";
            this.sentencesLabel.Size = new System.Drawing.Size(61, 13);
            this.sentencesLabel.TabIndex = 5;
            this.sentencesLabel.Text = "Sentences:";
            // 
            // paragraphsLabel
            // 
            this.paragraphsLabel.AutoSize = true;
            this.paragraphsLabel.Location = new System.Drawing.Point(545, 417);
            this.paragraphsLabel.Name = "paragraphsLabel";
            this.paragraphsLabel.Size = new System.Drawing.Size(64, 13);
            this.paragraphsLabel.TabIndex = 7;
            this.paragraphsLabel.Text = "Paragraphs:";
            // 
            // ttlNumTkns
            // 
            this.ttlNumTkns.AutoSize = true;
            this.ttlNumTkns.Location = new System.Drawing.Point(322, 417);
            this.ttlNumTkns.Name = "ttlNumTkns";
            this.ttlNumTkns.Size = new System.Drawing.Size(13, 13);
            this.ttlNumTkns.TabIndex = 8;
            this.ttlNumTkns.Text = "0";
            // 
            // ttlNumDWords
            // 
            this.ttlNumDWords.AutoSize = true;
            this.ttlNumDWords.Location = new System.Drawing.Point(431, 417);
            this.ttlNumDWords.Name = "ttlNumDWords";
            this.ttlNumDWords.Size = new System.Drawing.Size(13, 13);
            this.ttlNumDWords.TabIndex = 9;
            this.ttlNumDWords.Text = "0";
            this.ttlNumDWords.Click += new System.EventHandler(this.ttlNumDWords_Click);
            // 
            // ttlNumSents
            // 
            this.ttlNumSents.AutoSize = true;
            this.ttlNumSents.Location = new System.Drawing.Point(522, 417);
            this.ttlNumSents.Name = "ttlNumSents";
            this.ttlNumSents.Size = new System.Drawing.Size(13, 13);
            this.ttlNumSents.TabIndex = 10;
            this.ttlNumSents.Text = "0";
            this.ttlNumSents.Click += new System.EventHandler(this.ttlNumSents_Click);
            // 
            // ttlNumPgphs
            // 
            this.ttlNumPgphs.AutoSize = true;
            this.ttlNumPgphs.Location = new System.Drawing.Point(618, 417);
            this.ttlNumPgphs.Name = "ttlNumPgphs";
            this.ttlNumPgphs.Size = new System.Drawing.Size(13, 13);
            this.ttlNumPgphs.TabIndex = 11;
            this.ttlNumPgphs.Text = "0";
            // 
            // MenuForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(654, 434);
            this.Controls.Add(this.ttlNumPgphs);
            this.Controls.Add(this.ttlNumSents);
            this.Controls.Add(this.ttlNumDWords);
            this.Controls.Add(this.ttlNumTkns);
            this.Controls.Add(this.paragraphsLabel);
            this.Controls.Add(this.sentencesLabel);
            this.Controls.Add(this.dWordLabel);
            this.Controls.Add(this.tokensLabel);
            this.Controls.Add(this.fileToLoad);
            this.Controls.Add(this.fileLabel);
            this.Controls.Add(this.mainTabMenu);
            this.Controls.Add(this.topMenuStrip);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MenuForm";
            this.Text = "Text Analysis Program";
            this.Load += new System.EventHandler(this.welcomeForm_Load);
            this.mainTabMenu.ResumeLayout(false);
            this.originalText.ResumeLayout(false);
            this.originalText.PerformLayout();
            this.tokenPage.ResumeLayout(false);
            this.tokenPage.PerformLayout();
            this.dWordsPage.ResumeLayout(false);
            this.dWordsPage.PerformLayout();
            this.sentencesPage.ResumeLayout(false);
            this.sentencesPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sentenceNumBox)).EndInit();
            this.paragraphPage.ResumeLayout(false);
            this.paragraphPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.paragraphListBox)).EndInit();
            this.topMenuStrip.ResumeLayout(false);
            this.topMenuStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private TabControl mainTabMenu;
        private TabPage originalText;
        private TabPage tokenPage;
        private MenuStrip topMenuStrip;
        private ToolStripMenuItem fileToolStripMenuItem;
        private ToolStripMenuItem helpToolStripMenuItem;
        private TextBox originalTxtBox;
        private TextBox tokensBox;
        private TabPage dWordsPage;
        private TextBox dWordsBox;
        private TabPage sentencesPage;
        private TextBox sentencesBox;
        private TabPage paragraphPage;
        private TextBox paragraphBox;
        private ToolStripMenuItem aboutToolStripMenuItem;
        private Button sentencePrevBtn;
        private Button sentenceNextBtn;
        private Button paragraphPrevBtn;
        private Button paragraphNextBtn;
        private Label dsplSentNumLabel;
        private Label dsplParaNumLabel;
        private Label sentAvgWordLengthLabel;
        private Label sentNumWordsLabel;
        private TextBox sentenceAvgWordsBox;
        private TextBox sentenceNumWordsBox;
        private Label paraAvgWrdLabel;
        private Label paraNumSentLabel;
        private TextBox avgWordsBox;
        private TextBox numSentencesBox;
        private Label paraNumWordLabel;
        private TextBox pgphNumWordsBox;
        private Label fileLabel;
        private Label fileToLoad;
        private Label tokensLabel;
        private Label dWordLabel;
        private Label sentencesLabel;
        private Label paragraphsLabel;
        private Label ttlNumTkns;
        private Label ttlNumDWords;
        private Label ttlNumSents;
        private Label ttlNumPgphs;
        private NumericUpDown sentenceNumBox;
        private NumericUpDown paragraphListBox;
        private ToolStripMenuItem loadAFileToolStripMenuItem;
        private ToolStripMenuItem exitProgramToolStripMenuItem;
    }
}