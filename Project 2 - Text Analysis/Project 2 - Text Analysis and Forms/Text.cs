﻿//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//	Project:		Project 2 - Text Analysis and Forms
//	File Name:		Text.cs
//	Description:    Comprises the text file as a whole whilst in the program. It has token methods and also reads in the text
//                  from a filepath.
//	Course:			CSCI 2210-001 - Data Structures
//	Author:			Brandi Campbell, campbellb1@goldmail.etsu.edu
//	Created:		Thursday, February 21, 2016
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////


using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utils;


namespace TextAnalyzer
{

    /// <summary>
    /// This class collects, reads, and--to some degree--manipulates the content regarding tokenization.
    /// </summary>
    class Text
    {
        public string originalString;
        public List<string> stringList = new List<string>();


        /// <summary>
        /// Default Constructor
        /// Initializes a new instance of the <see cref="Text"/> class.
        /// </summary>
        public Text()
        {
            originalString = "";

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Text"/> class.
        /// </summary>
        /// <param name="path">The path.</param>
        public Text(string path)
        {
            
            try
            {
                // Create an instance of StreamReader to read from a file.
                // The using statement also closes the StreamReader.
     
                using (StreamReader sr = new StreamReader(path))
                {
                    while (sr.Peek() >= 0)
                    {
                        originalString += sr.ReadLine() + "\n";
                    }

                    sr.Close();
                }

            }
            catch (Exception e)
            {
                // Let the user know what went wrong.
                Console.WriteLine("The file could not be read:");
                Console.WriteLine(e.Message);
            }
           
            

        }


        /// <summary>
        /// Gets or sets the original string.
        /// </summary>
        /// <value>
        /// The original string.
        /// </value>
        public string OriginalString
        {
            get
            {
                return originalString;
            }

            set
            {
                originalString = value;
            }
        }


        /// <summary>
        /// Gets or sets the string list.
        /// </summary>
        /// <value>
        /// The string list.
        /// </value>
        public List<string> StringList
        {
            get
            {
                return stringList;
            }

            set
            {
                stringList = value;
            }
        }

        /// <summary>
        /// Makes the tokenized list of the text.
        /// </summary>
        public void mkTknText()
        {
            stringList = Utility.tokenize(this.originalString);
        }


        /// <summary>
        /// Returns the Tokenized string.
        /// </summary>
        /// <returns></returns>
        public List<string> returnTknString()
        {
            return stringList;
        }


        /// <summary>
        /// Prints the string list.
        /// </summary>
        /// <returns></returns>
        public string printStringList()
        {
            mkTknText();

           

            string output = "Tokens:    \n";
            int ordinal = 1;
            foreach(string s in stringList)
            {
                output += ordinal + ". " + s + "\n";
                ordinal++;
            }
            return output;
        }
    }
}
