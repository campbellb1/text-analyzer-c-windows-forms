﻿//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//	Project:		Project 2 - Text Analysis and Forms
//	File Name:		Driver.cs
//	Description:    Stores information about Users of the program and moderates the setting/getting of the aspects of Users.
//	Course:			CSCI 2210-001 - Data Structures
//	Author:			Brandi Campbell, campbellb1@goldmail.etsu.edu
//	Created:		Thursday, February 21, 2016
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

using TextAnalyzer;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Utils;


namespace TextAnalyzer
{


    /// <summary>
    /// This is the main driver for the Text Analysis Program. It creates an instance of the Welcome Form
    /// </summary>
    class Driver
    {
        [STAThread]

        ///<summary>
        ///This is the main drover for the Text Analysis Program
        ///This program generates a menu and allows the user to load in a text file
        ///and, via menu options, get information about the text.
        ///</summary>
        static void Main(string[] args)
        {

            WelcomeForm startScreen = new WelcomeForm(); //create a Welcome Form
            startScreen.ShowDialog(); //Begin the Windows Forms



        }
    }
}