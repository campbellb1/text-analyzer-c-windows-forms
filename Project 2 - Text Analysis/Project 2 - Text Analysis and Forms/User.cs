﻿//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//	Project:		Project 2 - Text Analysis and Forms
//	File Name:		User.cs
//	Description:    Stores information about Users of the program and moderates the setting/getting of the aspects of Users.
//	Course:			CSCI 2210-001 - Data Structures
//	Author:			Brandi Campbell, campbellb1@goldmail.etsu.edu
//	Created:		Thursday, February 21, 2016
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////



using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using Utils;


namespace TextAnalyzer
{
    /// <summary>
    ///  This is a class that encapsulates the user’s name, email address, and phone number.
    ///  Use regular expressions to validate the email address and phone number.
    /// </summary>
    public class User
    {
        public string stringName;
        public Name userName = new Name();    //holds the user's name
        private String telephone;  //holds the user's telephone number
        private String email; //holds the user's email address.

        /// <summary>
        /// Gets or sets the email property. Checks to ensure any set email is valid.
        /// </summary>
        /// <value>
        /// The email property.
        /// </value>
        public string emailProperty //The property for email (checks to ensure email is in proper format)
        {
            get
            {
                return email;
            }
            set
            {

           
                Match m = Regex.Match(value, @"([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})");
                
                
                    if (m.Success)
                    {

                        email = value;

                    }
        
                
            }
        }

        /// <summary>
        /// Gets or sets the telephone property. Checks to ensuer phone numbers to be set are valid.
        /// </summary>
        /// <value>
        /// The telephone property.
        /// </value>
        public string telephoneProperty //The property for email (checks to ensure email is in proper format)
        {
            get
            {

                return telephone;
            }
            set
            {

                Match m = Regex.Match(value, @"^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]\d{3}[\s.-]\d{4}$");
                
                    if (m.Success)
                    {

                        telephone = value;

                    }
             
               

            }

        }


        /// <summary>
        /// Gets or sets the name of the user.
        /// </summary>
        /// <value>
        /// The name of the user.
        /// </value>
        public Name UserName
        {
            get
            {
                return userName;
            }

            set
            {

                userName = value;
            }
        }

        /// <summary>
        /// Gets or sets the name of the string.
        /// </summary>
        /// <value>
        /// The name of the string.
        /// </value>
        public string StringName
        {
            get
            {
                return stringName;
            }

            set
            {
                stringName = value;
            }
        }




        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            userName.parseName();

            return "User Information:\n\nFirst Name: " + userName.RestOfName + "\nLast Name: " + userName.lastName + "\nSuffix: " + userName.Suffix + "\nPhone: " + telephone + "\nEmail: " + email;
        }
    }

 }

