﻿//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//	Project:		Project 2 - Text Analysis GUI
//	File Name:		Paragraph.cs
//	Description:    Stores information regarding individual Paragraphs
//	Course:			CSCI 2210-001 - Data Structures
//	Author:			Brandi Campbell, campbellb1@goldmail.etsu.edu
//	Created:		Thursday, February 21, 2016
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Utils;


namespace TextAnalyzer
{
    /// <summary>
    /// This class defines what a paragraph is within the Text Analysis program. It is a list of sentence objects.
    /// It keeps track of the number of sentences, words, avg sentence length, where the starting word location 
    /// and the last word's location. 
    /// </summary>
    class Paragraph
    {
        private string currentParagraph; //holds the string content of the paragraph
        public int numSentences; //number of sentences in the paragraph
        public int numWords;        //number of words in the paragraph
        public double avgSentenceLength;         //average sentence length throughout the paragraph
        private int startWord;         //ordinal location of the starting word.
        private int lastWord;           //ordinal locatoin of the final word.
        private SentenceList paragraphSentences;  //the list of Sentence objects in the Paragraph object



        /// <summary>
        /// Gets or sets the average length of the wd.
        /// </summary>
        /// <value>
        /// The average length of the wd.
        /// </value>
        public double AvgSentenceLength
        {
            get
            {
                return avgSentenceLength;
            }

            private set
            {
                avgSentenceLength = value;
            }
        }



        /// <summary>
        /// Gets or sets the number words.
        /// </summary>
        /// <value>
        /// The number words.
        /// </value>
        public int NumWords
        {
            get
            {
                return numWords;
            }

            private set
            {
                numWords = value;
            }
        }

        /// <summary>
        /// Gets or sets the paragraph sentences.
        /// </summary>
        /// <value>
        /// The paragraph sentences.
        /// </value>
        internal SentenceList ParagraphSentences
        {
            get
            {
                return paragraphSentences;
            }

            set
            {
                paragraphSentences = value;
            }
        }


        /// <summary>
        /// Gets or sets the number sentences.
        /// </summary>
        /// <value>
        /// The number sentences.
        /// </value>
        public int NumSentences
        {
            get
            {
                return numSentences;
            }

            private set
            {
                numSentences = value;
            }
        }


        /// <summary>
        /// Gets or sets the start word.
        /// </summary>
        /// <value>
        /// The start word.
        /// </value>
        public int StartWord
        {
            get
            {
                return startWord;
            }

            private set
            {
                startWord = value;
            }
        }



        /// <summary>
        /// Gets or sets the last word.
        /// </summary>
        /// <value>
        /// The last word.
        /// </value>
        public int LastWord
        {
            get
            {
                return lastWord;
            }

            private set
            {
                lastWord = value;
            }
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="Paragraph"/> class.
        /// </summary>
        /// <param name="content">The content.</param>
        /// <param name="lastPos">The last word's position.</param>
        public Paragraph(string content, int lastPos)
        {
            currentParagraph = content;
            List<string> wordList = Utility.tokenizeWords(currentParagraph); //cut the paragraph into words
            numWords = wordList.Count; //counts the number of words in the paragraph

            lastWord = lastPos;

            startWord = lastWord - NumWords;

            paragraphSentences = new SentenceList();
            paragraphSentences.Content = content;

            //provided this paragraph is not the first one, update its word position
            if (startWord != 0)
            {
                paragraphSentences.WordPos = startWord;
            }
            paragraphSentences.processList();

            
            numSentences = paragraphSentences.NumSentences;




            avgSentenceLength = numWords / (double)numSentences; //take the average sentence length and round to 2 decimal pts.
            avgSentenceLength = Math.Round(avgSentenceLength, 2);
        }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            string output = currentParagraph + "\n\n";

            return output;


        }
    }
}

