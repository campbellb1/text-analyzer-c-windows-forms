﻿//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//	Project:		Project 2 - Text Analysis and Forms
//	File Name:		WelcomeForm.cs
//	Description:    Serves as the Welcome Message for the program and also allows input for user information
//	Course:			CSCI 2210-001 - Data Structures
//	Author:			Brandi Campbell, campbellb1@goldmail.etsu.edu
//	Created:		Monday, March 14, 2016
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

using System.Windows.Forms;
using TextAnalyzer;

namespace TextAnalyzer
{


    /// <summary>
    /// This class functions as the Welcome Screen / User Information Input
    /// </summary>
    /// <seealso cref="System.Windows.Forms.Form" />
    public partial class WelcomeForm : Form
    {
        User newUser = new User(); //Will hold user info.
        bool mainFormLoaded = false;


        /// <summary>
        /// Gets or sets the new user.
        /// </summary>
        /// <value>
        /// The new user.
        /// </value>
        internal User NewUser
        {
            get
            {
                return newUser;
            }

            set
            {
                newUser = value;
            }
        }


        /// <summary>
        /// Initializes the splash screen, holds it for 5 seconds and then closes it.
        /// </summary>
        public WelcomeForm()
        {

            Thread t = new Thread(new ThreadStart(SplashStart));
            t.Start();
            Thread.Sleep(5000);

            InitializeComponent();

            t.Abort();

        }


        /// <summary>
        /// Splashes the start.
        /// </summary>
        public void SplashStart()
        {
            Application.Run(new SplashForm());
        }



       



        /// <summary>
        /// Handles the Click event of the welcomeLabel control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void welcomeLabel_Click(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Exits the BTN click.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void ExitBtnClick(object sender, EventArgs e)
        {

            //check to see if user really wants to quit
            switch (MessageBox.Show(this, "Thank you for (almost) using the Text Analyzer Sir / Madam." + "\nAre you sure you want to close, wihout analyzing anything ? ", "Closing", MessageBoxButtons.YesNo))
            {
                case DialogResult.No:

                    break;
                default:
                    Environment.Exit(0);
                    break;
            }
        }



        /// <summary>
        /// This method manages what happends when the user clicks the START button. So long as the user has entered
        /// valid data into the name, email, and telephone boxes, it will build an instance of the MenuForm (Main Menu).
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void StartBtnClick(object sender, EventArgs e)
        {
            //if there is any information already entered into the info boxes, put them into their respective
            //variables within newUser
            newUser.userName.unparsedName = nameBox.Text;
            newUser.emailProperty = emailBox.Text;
            newUser.telephoneProperty = phoneBox.Text;



            //From here on down, an extended if-else construct checks to see if there is valid data entered into 
            //each box, if there is, but that data into the respective variable and proceed to the next attribute.
            //------------------------------------------------------------------------------------------------

            if (newUser.userName.Equals("")) //make sure the user actually enters a name.
            {


                if (nameBox.Text.Equals(""))
                {
                    MessageBox.Show("Please Enter a Name...:");
                }


            }

            else
            {
                newUser.userName.unparsedName = nameBox.Text;
                newUser.userName.parseName();
                



                //prompt for user information

                if (newUser.telephoneProperty == null)
                {
                    MessageBox.Show("Please Enter a Telephone Number with Area Code in the format (###-###-####): ");
                }
                else
                {
                    newUser.telephoneProperty = phoneBox.Text;

                    if (newUser.emailProperty == null)
                    {
                        MessageBox.Show("Please Enter a VALID Email: ");
                    }
                    else
                    {
                        newUser.emailProperty = emailBox.Text;

                        //Transition to the Main Menu * * * * * 
                        mainFormLoaded = true;
                        MenuForm myMenu = new MenuForm(newUser);
                        //Transition to the Main Menu * * * * * 


                    }
                }
            }
        }

        /// <summary>
        /// Handles the Load event of the WelcomeForm control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void WelcomeForm_Load(object sender, EventArgs e)
        {

        }



        /// <summary>
        /// Handles the TextChanged event of the nameBox control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void nameBox_TextChanged(object sender, EventArgs e)
        {

        }



        /// <summary>
        /// Handles the TextChanged event of the phoneBox control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void phoneBox_TextChanged(object sender, EventArgs e)
        {

        }


        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Form.FormClosing" /> event.
        /// </summary>
        /// <param name="e">A <see cref="T:System.Windows.Forms.FormClosingEventArgs" /> that contains the event data.</param>
        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            if (!mainFormLoaded) //if the Main Form is not loaded...
            {
                base.OnFormClosing(e);

            if (e.CloseReason == CloseReason.WindowsShutDown) return;

            // Confirm user wants to close
            
                switch (MessageBox.Show(this, "Thank you for (almost) using the Text Analyzer Sir/Madam." + "\nAre you sure you want to close, wihout analyzing anything?", "Closing", MessageBoxButtons.YesNo))
                {
                    case DialogResult.No:
                        e.Cancel = true;
                        break;
                    default:
                        break;
                }
            }
        }
    }
}
