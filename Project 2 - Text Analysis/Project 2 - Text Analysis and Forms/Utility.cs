﻿//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//	Project:		Project 2 - Text Analysis and Forms
//	File Name:		Utility.cs
//	Description:    Serves as a general-purpose utility toolkit that stores useful methods
//	Course:			CSCI 2210-001 - Data Structures
//	Author:			Brandi Campbell, campbellb1@goldmail.etsu.edu
//	Created:		Thursday, February 21, 2016
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Utils;


namespace Utils
{

    /// <summary>
    /// This class contains useful methods that can be used in many programs. Versatile.
    /// </summary>
    class Utility
    {

        /// <summary>
        /// Tokenizes the specified text.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <returns></returns>
        public static List<string> tokenize(string text)
        {
            string tokenizedString = "";

            List<string> wordList = new List<string>(); //the list of tokens will be put into here.


            Match match = Regex.Match(text, "(\\w+'*\\w*)+|[.,\\/#!$%\\^&\\*;:{}=\\-_`~'()\n]|\r", //this match matches words & numbers
                 RegexOptions.IgnoreCase);




            //when a match is found, add it to the list.
            while (match.Success)
            {
                if (match.Value.Equals("\n"))
                {
                    wordList.Add("<newline>");
                    

                }
                else if (match.Value.Equals("\r"))
                {
                    wordList.Add("<carriage return>");
                }
                else
                {
                    wordList.Add(match.Value);
                    
                }
                match = match.NextMatch();
            }

            
            return wordList;
        }




        /// <summary>
        /// Tokenizes the words.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <returns></returns>
        public static List<String> tokenizeWords(String text)
        {
            String tokenizedString = "";

            List<string> wordList = new List<string>();

            Match match = Regex.Match(text, "(\\w+'*\\w*)+",
                 RegexOptions.IgnoreCase);

            
            {
                while (match.Success)
                {
                    if (match.Success)
                    {
                        wordList.Add(match.Value);
                    }
                    
                    match = match.NextMatch();
                }
                
            }

            return wordList;
        }




        /// <summary>
        /// Skips the specified number to skip.
        /// </summary>
        /// <param name="numToSkip">The number to skip.</param>
        public static void skip(int numToSkip)
        {
            for(int i = 0; i<numToSkip; i++)
            {
                Console.WriteLine("\n");
            }
        }
    }
    
}
