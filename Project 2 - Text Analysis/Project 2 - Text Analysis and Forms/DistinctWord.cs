﻿//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//	Project:		Project 2 - Text Analysis GUI
//	File Name:		User.cs
//	Description:    Manages individual (distinct) words and their properties within the program
//	Course:			CSCI 2210-001 - Data Structures
//	Author:			Brandi Campbell, campbellb1@goldmail.etsu.edu
//	Created:		Thursday, February 21, 2016
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utils;


/// <summary>
/// 
/// </summary>
namespace TextAnalyzer
{
 
    /// <summary>
    /// This class holds unique words and allows them to be compared to the text for their occurrences to be documented.
    /// </summary>
    /// <seealso cref="System.IComparable{Project1.DistinctWord}" />
    /// <seealso cref="System.IEquatable{Project1.DistinctWord}" />
    class DistinctWord : IComparable<DistinctWord>, IEquatable<DistinctWord>
    {

        string dWord; //This variable holds the Distinct Word's string content.
        int occurrences = 0; //This integer holds the number of times the distinct word occurs


        /// <summary>
        /// Gets or sets the distinct word.
        /// </summary>
        /// <value>
        /// The distinct word.
        /// </value>
        public string DWord
        {
            get
            {
                return dWord;
            }

            set
            {
                dWord = value;
            }
        }

        /// <summary>
        /// Gets or sets the number of occurrences.
        /// </summary>
        /// <value>
        /// The occurrences.
        /// </value>
        public int Occurrences
        {
            get
            {
                return occurrences;
            }

            set
            {
                occurrences = value;
            }
        }

        /// <summary>
        /// Default Constructor
        /// Initializes a new instance of the <see cref="DistinctWord"/> class.
        /// </summary>
        public DistinctWord()
        {
            dWord = "";
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DistinctWord"/> class.
        /// </summary>
        /// <param name="inputWord">The input word.</param>
        public DistinctWord(String inputWord)
        {
            dWord = inputWord.ToLower();
        }

        /// <summary>
        /// Compares the current object with another object of the same type.
        /// </summary>
        /// <param name="other">An object to compare with this object.</param>
        /// <returns>
        /// A value that indicates the relative order of the objects being compared. The return value has the following meanings: Value Meaning Less than zero This object is less than the <paramref name="other" /> parameter.Zero This object is equal to <paramref name="other" />. Greater than zero This object is greater than <paramref name="other" />.
        /// </returns>
        public int CompareTo(DistinctWord other)
        {
          
            int result = this.dWord.CompareTo(other.dWord);
        
               
            return result;
        }


        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <param name="other">An object to compare with this object.</param>
        /// <returns>
        /// true if the current object is equal to the <paramref name="other" /> parameter; otherwise, false.
        /// </returns>
        public bool Equals(DistinctWord other)
        {
            bool equality = this.dWord.Equals(other.DWord);
            return equality;
        }


        /// <summary>
        /// Counts the instances of a distinct word in the text.
        /// </summary>
        /// <param name="text">The whole text for the distinct word to be compared to.</param>
        public void countInstances(string text)
        {
            
            List<string> wordList = Utility.tokenizeWords(text);
            foreach (string word in wordList)
            {
                if (dWord.Equals(word.ToLower()))
                {
                    occurrences++;
                }
            }
        }

        /// <summary>
        /// Returns a string that contains the distinct word and its number of occurrences
        /// </summary>
        /// <returns>
        /// A string that represents this instance.
        /// </returns>
        override public string ToString() //override the default ToString
        {
            

            return dWord.PadRight(16) + occurrences + "\n";
            
        }
    }

    

    }

