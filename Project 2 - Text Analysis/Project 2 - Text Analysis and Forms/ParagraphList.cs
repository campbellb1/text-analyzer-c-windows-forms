﻿//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//	Project:		Project 2 - Text Analysis and Forms
//	File Name:		ParagraphList.cs
//	Description:    Stores information about all the paragraphs of the program and moderates the setting/getting of the aspects of Users.
//	Course:			CSCI 2210-001 - Data Structures
//	Author:			Brandi Campbell, campbellb1@goldmail.etsu.edu
//	Created:		Thursday, February 21, 2016
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////




using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Utils;


namespace TextAnalyzer
{


    /// <summary>
    /// This class manages all the paragraphs in the Text Analyzer. It is composed of Paragraph objects. 
    /// The number of words, paragraphs, and sentences are tracked, along with the average length of 
    /// the sentences of all paragraphs.
    /// </summary>
    class ParagraphList
    {
        private int wordPos = 0;
        private double avgLength = 0;
        private int numParagraphs = 0;
        private int numWords = 0;
        private int numSentences = 0;
        List<Paragraph> pList = new List<Paragraph>();

        /// <summary>
        /// Gets or sets the p list.
        /// </summary>
        /// <value>
        /// The p list.
        /// </value>
        internal List<Paragraph> PList
        {
            get
            {
                return pList;
            }

            private set
            {
                pList = value;
            }
        }


        /// <summary>
        /// Gets or sets the average length.
        /// </summary>
        /// <value>
        /// The average length.
        /// </value>
        public double AvgLength
        {
            get
            {
                return avgLength;
            }

            private set
            {
                avgLength = value;
            }
        }



        /// <summary>
        /// Gets the number paragraphs.
        /// </summary>
        /// <value>
        /// The number paragraphs.
        /// </value>
        public int NumParagraphs
        {
            get
            {
                return numParagraphs;
            }

            private set
            {
                numParagraphs = value;
            }
        }


        /// <summary>
        /// Gets the number words.
        /// </summary>
        /// <value>
        /// The number words.
        /// </value>
        public int NumWords
        {
            get
            {
                return numWords;
            }

            private set
            {
                numWords = value;
            }
        }


        /// <summary>
        /// Gets the number sentences.
        /// </summary>
        /// <value>
        /// The number sentences.
        /// </value>
        public int NumSentences
        {
            get
            {
                return numSentences;
            }

            private set
            {
                numSentences = value;
            }
        }





        /// <summary>
        /// Initializes a new instance of the <see cref="SentenceList"/> class.
        /// </summary>
        /// <param name="input">The input.</param>
        public ParagraphList(Text input)
        {
            //Take the string from the Text and find text that matches a "Paragraph" form.
            MatchCollection matchList = Regex.Matches(input.OriginalString, @"[^\r\n]+((\r|\n|\r\n)[^\r\n]+)*");
            var paraList = matchList.Cast<Match>().Select(match => match.Value).ToList();



            numParagraphs = paraList.Count;

            //Put each paragraph found within a Paragraph object.
            foreach (string s in paraList)
            {
                List<string> tempSentence = Utility.tokenizeWords(s);
                wordPos += tempSentence.Count;
                Paragraph paragraphToAdd = new Paragraph(s, wordPos);
                numWords +=  paragraphToAdd.NumWords;
                numSentences += paragraphToAdd.NumSentences;
                pList.Add(paragraphToAdd);
            }
           

            avgLength = numWords / (double)numSentences; //calculate the average sentence length
            avgLength = Math.Round(avgLength, 2);

        }


        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            int paragraphNum = 1;
            string output = "Paragraphs in the Text: \n\n\n";
            foreach (Paragraph p in pList)
            {
                output += ("Paragraph " + paragraphNum + ". \n\n" +
                    p.ToString() + "\n\n");
                paragraphNum++;
            }

            output += "---------------------------------------------------------------------------------------------" +
                "\nThere are " + numSentences + " sentences with an average length of " + Math.Round(avgLength,2) + " words.\n";

            return output;
        }
    }
}
