﻿//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//	Project:		Project 2 - Text Analysis GUI
//	File Name:		Name.cs
//	Description:    Manages and stores names and their components
//	Course:			CSCI 2210-001 - Data Structures
//	Author:			Brandi Campbell, campbellb1@goldmail.etsu.edu
//	Created:		Thursday, February 21, 2016
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Utils;


namespace TextAnalyzer
{
    /// <summary>
    /// This class manages names by allowing them to be split into last names, suffixes, and "restofname"
    /// </summary>
    public class Name
    {
        public string unparsedName; //holds the original string, unchanged
        private string restOfName; //The first name & middle name (if applicable)
        private string suffix;            //The suffix (ie. jr, sr, III, etc.)

        /// <summary>
        /// Gets or sets the last name.
        /// </summary>
        /// <value>
        /// The last name.
        /// </value>
        public string lastName;     //The last name


        /// <summary>
        /// Gets or sets the name of the rest of.
        /// </summary>
        /// <value>
        /// The name of the rest of.
        /// </value>
        public string RestOfName
        {
            get
            {
                return restOfName;
            }

            set
            {
                restOfName = value;
               
            }
        }


        /// <summary>
        /// Gets or sets the suffix.
        /// </summary>
        /// <value>
        /// The suffix.
        /// </value>
        public string Suffix
        {
            get
            {
                return suffix;
            }

            set
            {
                suffix = value;
            }
        }

        public string LastName
        {
            get
            {
                return lastName;
            }

            set
            {
                lastName = value;
            }
        }


        /// <summary>
        /// Parses the name into its different parts.
        /// </summary>
        /// <param name="input">The Full Name, unedited.</param>
        /// <returns></returns>
        public void parseName()
        {
            //finds the positions of the first/last spaces/commas by using IndexOf / LastIndexOf
            int firstSpace = unparsedName.IndexOf(" ");
            int lastSpace = unparsedName.LastIndexOf(" ");
            int firstComma = unparsedName.IndexOf(",");
            int lastComma = unparsedName.LastIndexOf(",");
            
           
            string parsedName = "";

            //if there is no comma...set the last name and first name based on the first space
            if (firstComma < 0)
            {
                try
                {
                    restOfName = unparsedName.Substring(0, firstSpace);
                    lastName = unparsedName.Substring(firstSpace + 1);
                }
                catch(Exception firstNameOnly)
                {
                    restOfName = unparsedName;
                }

            }

            //if there is only one comma...
            else if (firstComma == lastComma)
            {

                //if the first comma comes before the first space
                if (firstComma < firstSpace)
                {
                    //interpret the name as such: Last Name, RestOfName
                    lastName = unparsedName.Substring(0, firstComma);
                    restOfName = unparsedName.Substring(firstComma + 1);

                }
                else
                {
                    //interpret the name as such: RestOfName LastName, Suffix
                    restOfName = unparsedName.Substring(0, firstSpace);
                    lastName = unparsedName.Substring(firstSpace + 1, firstComma - firstSpace - 1);
                    suffix = unparsedName.Substring(lastComma + 1);

                }

            }
            //if there are two commas
            else if (firstComma < lastComma)
            {
                //interpret the name as such: LastName, RestOfName, Suffix
                lastName = unparsedName.Substring(0, firstComma + 1);
                restOfName = unparsedName.Substring(firstComma + 1, lastComma - firstComma - 1);
                suffix = unparsedName.Substring(lastComma + 1);

            }
            try
            {
                lastName = char.ToUpper(lastName[0]) + lastName.Substring(1);
            }
            catch(Exception firstNameOnly)
            {
                //only first name was entered
            }
            restOfName = char.ToUpper(restOfName[0]) + restOfName.Substring(1);


            parsedName = restOfName + " " + lastName;
         

        }

       
    }
}
